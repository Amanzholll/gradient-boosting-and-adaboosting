import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.linear_model import SGDRegressor
from sklearn.metrics import mean_squared_error
from sklearn.impute import SimpleImputer

data = pd.read_csv('hdi.csv')

data_cleaned = data.dropna(subset=['indicator_value_num'])

features = ['indicator_value_num']
X = data_cleaned[features]
y = data_cleaned['indicator_value_num']

imputer = SimpleImputer(strategy='mean')
X_imputed = imputer.fit_transform(X)

X_train, X_test, y_train, y_test = train_test_split(X_imputed, y, test_size=0.2, random_state=42)

model = SGDRegressor(max_iter=1000, tol=1e-3)
model.fit(X_train, y_train)

predictions = model.predict(X_test)

mse = mean_squared_error(y_test, predictions)
print('Mean Squared Error:', mse)